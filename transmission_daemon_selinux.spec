# vim: sw=4:ts=4:et


%define relabel_files() \
restorecon -R /usr/bin/transmission-daemon; \
restorecon -R /usr/lib/systemd/system/transmission-daemon.service; \
restorecon -R /var/lib/transmission; \

%define selinux_policyver 3.13.1-229

Name:   transmission_daemon_selinux
Version:	1.0
Release:	1%{?dist}
Summary:	SELinux policy module for transmission_daemon

Group:	System Environment/Base		
License:	GPLv2+	
# This is an example. You will need to change it.
URL:		http://HOSTNAME
Source0:	transmission_daemon.pp
Source1:	transmission_daemon.if
Source2:	transmission_daemon_selinux.8


Requires: policycoreutils, libselinux-utils
Requires(post): selinux-policy-base >= %{selinux_policyver}, policycoreutils
Requires(postun): policycoreutils
Requires(post): transmission-daemon
BuildArch: noarch

%description
This package installs and sets up the  SELinux policy security module for transmission_daemon.

%install
install -d %{buildroot}%{_datadir}/selinux/packages
install -m 644 %{SOURCE0} %{buildroot}%{_datadir}/selinux/packages
install -d %{buildroot}%{_datadir}/selinux/devel/include/contrib
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/selinux/devel/include/contrib/
install -d %{buildroot}%{_mandir}/man8/
install -m 644 %{SOURCE2} %{buildroot}%{_mandir}/man8/transmission_daemon_selinux.8
install -d %{buildroot}/etc/selinux/targeted/contexts/users/


%post
semodule -n -i %{_datadir}/selinux/packages/transmission_daemon.pp
if /usr/sbin/selinuxenabled ; then
    /usr/sbin/load_policy
    %relabel_files

fi;
exit 0

%postun
if [ $1 -eq 0 ]; then
    semodule -n -r transmission_daemon
    if /usr/sbin/selinuxenabled ; then
       /usr/sbin/load_policy
       %relabel_files

    fi;
fi;
exit 0

%files
%attr(0600,root,root) %{_datadir}/selinux/packages/transmission_daemon.pp
%{_datadir}/selinux/devel/include/contrib/transmission_daemon.if
%{_mandir}/man8/transmission_daemon_selinux.8.*


%changelog
* Mon Sep  9 2019 YOUR NAME <YOUR@EMAILADDRESS> 1.0-1
- Initial version

